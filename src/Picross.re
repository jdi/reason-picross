let ste = ReasonReact.stringToElement;

let module Square = {
    type state = {
        realValue : bool,
        inputValue: int
    };

    let verifyInputValue state => {
        if (state.realValue) {
            (state.inputValue != 1) ? 1 : 0
        } else {
            2
        }
    };

    type action =
      | Fill
      | Cross;

    let component = ReasonReact.reducerComponent "Square";
    let make ::realValue _children => {
        ...component,
        initialState: fun () => {realValue: realValue, inputValue: 0},
        reducer: fun action state => switch action {
            | Fill => ReasonReact.Update {...state, inputValue: verifyInputValue state}
            | Cross => ReasonReact.Update {...state, inputValue: (state.inputValue != 2) ? 2 : 0}
        },
        render: fun {state: {inputValue}, reduce} => {
            let classN = switch inputValue {
                | 1 => "Square Square--Filled"
                | 2 => "Square Square--Crossed"
                | _ => "Square"
            };
            <div
                className=classN
                onClick=(reduce (fun evt => (ReactEventRe.Mouse.altKey evt) ? Cross : Fill))
            />
        }
    }
};

let list = [true, true, false, false, false];

let component = ReasonReact.statelessComponent "Picross";
let make _children => {
    ...component,
    render: fun _self => {
        <div>
            (List.map (fun b => <Square realValue=b />) list
                |> Array.of_list
                |> ReasonReact.arrayToElement
            )
        </div>
    }
};
